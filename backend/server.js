/* eslint-disable @typescript-eslint/no-var-requires */
const express = require('express');
const moment = require('moment');

const app = express();
const port = 5000;
const panels = require('./data');

function random(min, max) {
  return (Math.random() * (max - min + 1) + min).toFixed(2);
}

app.get('/panels', (req, res) => {
  const freshData = [];
  panels.data.forEach((panel) => {
    freshData.push({
      ...panel,
      time: moment().toISOString(),
      wattage: random(0, 400),
      voltage: random(0, 20),
    });
  });
  res.send(freshData);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
