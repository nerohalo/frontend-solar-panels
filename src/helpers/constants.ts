/* eslint-disable no-useless-escape */
export default {
  ACTION_TYPES: {
    PANELS_SET: 'PANELS_SET',
    PANELS_UPDATE: 'PANELS_UPDATE',
  },
} as const;
