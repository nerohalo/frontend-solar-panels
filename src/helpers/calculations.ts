export const convertWattageToKWH = (wattage: number) => wattage / 1000;
