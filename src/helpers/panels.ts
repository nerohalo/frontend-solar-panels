export const isPanelWeak = (wattage: number, voltage: number) => voltage < 10 && wattage < 200;
