import { PanelsService } from 'types';

import api from 'helpers/apiInstance';

export const panelsService: PanelsService = {
  fetchPanels: () => api.get('/panels', {}),
};
