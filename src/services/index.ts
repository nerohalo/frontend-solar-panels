import { ServiceProvider } from 'types';

import { panelsService } from './panelsService';

const serviceProvider: ServiceProvider = {
  panelsService,
};

export default serviceProvider;
