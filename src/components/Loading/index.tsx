import React from 'react';

import styles from './Loading.module.scss';

const Loading = ({ ...props }: React.HTMLAttributes<HTMLDivElement>) => (
  <div className={styles.loading} {...props}>
    ...loading
  </div>
);

export default Loading;
