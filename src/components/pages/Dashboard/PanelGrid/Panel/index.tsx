import classNames from 'classnames';
import React, { useCallback, useMemo } from 'react';

import { Panel as PanelType } from 'types';

import { isPanelWeak } from 'helpers/panels';

import styles from './Panel.module.scss';

type Props = {
  panel: PanelType
}

const Panel = ({ panel }: Props) => {
  const panelIsWeak = useMemo(() => isPanelWeak(Number(panel.wattage), Number(panel.voltage)), [panel]);
  const getPanelAppearance = useCallback(() => {
    if (panelIsWeak) {
      return 'negative';
    }
    return 'positive';
  }, [panelIsWeak]);

  const getPanelAriaHealthLabel = useCallback(() => {
    if (panelIsWeak) {
      return 'weak';
    }
    return 'healthy';
  }, [panelIsWeak]);

  return (
    <div
      aria-label={`${getPanelAriaHealthLabel()} panel number ${panel.id}`}
      className={classNames(styles.container, styles[getPanelAppearance()])}
      data-testid="rendered-panel"
    >
      <div aria-hidden className={styles.identifier}>#{panel.id}</div>

      <div className={styles.infoContainer}>
        <div aria-label={`current panel wattage is ${panel.wattage}`} className={styles.unit}>{panel.wattage} W</div>
        <div aria-label={`current panel voltage is ${panel.voltage}`} className={styles.unit}>{panel.voltage} V</div>
      </div>
    </div>
  );
};

export default Panel;
