import React from 'react';
import { useSelector } from 'react-redux';

import { ApplicationState } from 'types';

import Loading from 'components/Loading';

import Panel from './Panel';

import styles from './PanelGrid.module.scss';

const PanelGrid = () => {
  const { data: panels, loading } = useSelector((state: ApplicationState) => state.panels);

  if (loading) {
    return <Loading data-testid="panel-grid-loading" />;
  }

  return (
    <main className={styles.container} aria-label="panel grid" data-testid="panel-grid">
      {panels.map((panel) => (
        <Panel key={panel.id} panel={panel}/>
      ))}
    </main>
  );
};

export default PanelGrid;
