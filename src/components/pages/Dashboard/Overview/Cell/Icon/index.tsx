import classNames from 'classnames';
import React from 'react';

import styles from '../Cell.module.scss';

const Icon = ({ iconName }: { iconName: string }) => (
  <i className={classNames('material-icons-outlined', styles.icon)} aria-hidden>{iconName}</i>
);

export default Icon;
