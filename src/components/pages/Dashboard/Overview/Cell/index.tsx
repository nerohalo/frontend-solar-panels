import classNames from 'classnames';
import React from 'react';

import Icon from './Icon';
import Title from './Title';
import Unit from './Unit';

import styles from './Cell.module.scss';

type Props = {
  appearance?: 'negative' | 'positive' | 'informative'
  fluid?: boolean,
}

interface CellComponent extends React.FC<Props & React.HTMLAttributes<HTMLDivElement>> {
  Title: typeof Title;
  Icon: typeof Icon;
  Unit: typeof Unit;
}

const Cell: CellComponent = ({
  children,
  appearance = 'informative',
  fluid = true,
  ...props
}) => (
  <div className={classNames(styles.container, styles[appearance], { [styles.fluid]: fluid })} {...props}>
    {children}
  </div>
);

Cell.Title = Title;
Cell.Icon = Icon;
Cell.Unit = Unit;

export default Cell;
