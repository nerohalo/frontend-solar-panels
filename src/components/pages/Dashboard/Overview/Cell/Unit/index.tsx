import React from 'react';

import styles from '../Cell.module.scss';

const Unit = ({ children }: { children: React.ReactNode }) => (
  <div className={styles.unit}>{children}</div>
);

export default Unit;
