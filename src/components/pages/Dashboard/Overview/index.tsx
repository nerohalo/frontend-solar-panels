import React from 'react';
import { useSelector } from 'react-redux';

import { getOverviewData } from 'selectors/panels';

import Cell from './Cell';

import styles from './Overview.module.scss';

const Overview = () => {
  const {
    totalKWH,
    strongHealthPanels,
    weakHealthPanels,
    lastUpdated,
  } = useSelector(getOverviewData);

  return (
    <header aria-label="header" className={styles.container} data-testid="overview-container">
      <div aria-label="park overview" className={styles.panelOverviewContainer}>
        <h1 aria-label="overview" className={styles.title}>Overview</h1>

        <div className={styles.cellContainer}>
          <Cell aria-label="total kilowatt hours cell" data-testid="overview-total-kwh-cell">
            <Cell.Icon iconName="bolt"/>
            <Cell.Title>Total KW/H:</Cell.Title>
            <Cell.Unit>{totalKWH}</Cell.Unit>
          </Cell>

          <Cell aria-label="updated at cell" fluid={false} data-testid="overview-updated-at-cell">
            <Cell.Icon iconName="update" />
            <Cell.Title>Updated at:</Cell.Title>
            <Cell.Unit>{lastUpdated}</Cell.Unit>
          </Cell>
        </div>
      </div>

      <div aria-label="park health overview" className={styles.panelHealthContainer}>
        <h2 aria-label="park health" className={styles.title}>Park Health</h2>

        <div className={styles.cellContainer}>
          <Cell aria-label="healthy panels cell" appearance="positive" data-testid="overview-healthy-panels-cell">
            <Cell.Icon iconName="check_circle" />
            <Cell.Title>Healthy panels:</Cell.Title>
            <Cell.Unit>{strongHealthPanels}</Cell.Unit>
          </Cell>

          <Cell aria-label="weak panels cell" appearance="negative" data-testid="overview-weak-panels-cell">
            <Cell.Icon iconName="report_problem" />
            <Cell.Title>Weak panels:</Cell.Title>
            <Cell.Unit>{weakHealthPanels}</Cell.Unit>
          </Cell>
        </div>
      </div>
    </header>
  );
};

export default Overview;
