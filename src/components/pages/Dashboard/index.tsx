import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { DispatchExts } from 'types';

import * as panelsActions from 'actions/panels';

import Overview from './Overview';
import PanelGrid from './PanelGrid';

import styles from './Dashboard.module.scss';

const Dashboard = () => {
  const dispatch = useDispatch<DispatchExts>();
  useEffect(() => {
    dispatch(panelsActions.fetchPanels()).then(() => {
      setInterval(() => {
        dispatch(panelsActions.fetchPanels());
      }, 5000);
    });
  }, [dispatch]);

  return (
    <div className={styles.container} data-testid="dashboard">
      <Overview />
      <PanelGrid />
    </div>
  );
};

export default Dashboard;
