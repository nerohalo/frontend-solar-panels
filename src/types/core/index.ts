export * from './misc';

export type Panel = {
  id: number,
  date: string,
  time: string,
  energy: string,
  efficiency: string,
  wattage: string,
  voltage: string,
};
