import { Action } from 'redux';
import { ThunkAction, ThunkDispatch, ThunkMiddleware } from 'redux-thunk';

import { ApplicationState, ServiceProvider } from 'types';

export type ThunkResult<R> = ThunkAction<R, ApplicationState, ServiceProvider, Action>;
export type DispatchExts = ThunkDispatch<ApplicationState, ServiceProvider, Action>;
export type ThunkMiddlewareType = ThunkMiddleware<ApplicationState, Action, ServiceProvider>
