import { Panel } from 'types';

export type PanelsReduxProps = {
  data: Panel[]
  loading: boolean;
}
