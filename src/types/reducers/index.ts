import { PanelsReduxProps } from 'types';

export * from './panels';

export interface ApplicationState {
  panels: PanelsReduxProps;
}
