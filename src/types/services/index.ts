import { Panel } from 'types';

export type PanelsService = {
  fetchPanels: () => Promise<Panel[]>;
}

export type ServiceProvider = {
  panelsService: PanelsService;
}
