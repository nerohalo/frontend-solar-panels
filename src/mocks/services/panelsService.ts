import { Mock, PanelsService } from 'types';

import { panelFixture } from 'fixtures';

export const mockPanelsService: Mock<PanelsService> = (customMocks) => ({
  fetchPanels: async () => [panelFixture()],
  ...customMocks,
});
