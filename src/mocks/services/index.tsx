import { ServiceProvider, Mock } from 'types';

import { mockPanelsService } from 'mocks';

export const mockServiceProvider: Mock<ServiceProvider> = (customMocks = {}) => ({
  panelsService: mockPanelsService(),
  ...customMocks,
});

export * from './panelsService';
