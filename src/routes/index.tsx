import React, { lazy } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Loading from 'components/Loading';
import PageNotFound from 'components/pages/NotFound';

const Dashboard = lazy(() => import('components/pages/Dashboard'));

const Router = () => (
  <BrowserRouter>
    <Routes>
      <Route
        path="/"
        element={
          <React.Suspense fallback={(<Loading />)}>
            <Dashboard />
          </React.Suspense>
        }
      />

      <Route path="*" element={<PageNotFound />} />
    </Routes>
  </BrowserRouter>
);

export default Router;
