import { createAction } from 'typesafe-actions';

import { ThunkResult, Panel } from 'types';

import c from 'helpers/constants';

export const panelsSet = createAction(c.ACTION_TYPES.PANELS_SET)<Panel[]>();

export const fetchPanels = (): ThunkResult<Promise<void>> => async (
  dispatch, getState, services,
): Promise<void> => services.panelsService.fetchPanels().then((panels) => {
  dispatch(panelsSet(panels));
});
