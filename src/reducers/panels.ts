import { ActionType, createReducer } from 'typesafe-actions';

import { PanelsReduxProps } from 'types';

import * as panelsActions from 'actions/panels';

export const defaultState: PanelsReduxProps = {
  data: [],
  loading: true,
};

export const reducer = createReducer<PanelsReduxProps, ActionType<typeof panelsActions>>(defaultState)
  .handleAction(panelsActions.panelsSet, (state, action) => ({
    ...state,
    data: action.payload,
    loading: false,
  }));

export default reducer;
