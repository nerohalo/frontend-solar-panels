import { combineReducers } from 'redux';

import { ApplicationState } from 'types';

import panels from './panels';

const mainReducer = combineReducers<ApplicationState>({
  panels,
});

export default mainReducer;
