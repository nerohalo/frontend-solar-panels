import { Fixture, Panel } from 'types';

export const panelFixture: Fixture<Panel> = (state = {}) => ({
  id: 124,
  date: '2020-09-24',
  time: '18:00:00',
  energy: '3,078.600kWh',
  efficiency: '5.131kWh/kW',
  wattage: '250',
  voltage: '8033',
  ...state,
});
