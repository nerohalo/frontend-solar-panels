import { Fixture, ApplicationState } from 'types';

import { panelsReducerStateFixture } from 'fixtures';

export const rootReducerStateFixture: Fixture<ApplicationState> = (state = {}) => ({
  panels: panelsReducerStateFixture(),
  ...state,
});

export * from './panels';
