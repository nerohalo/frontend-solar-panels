import { Fixture, PanelsReduxProps } from 'types';

import { panelFixture } from 'fixtures';

export const panelsReducerStateFixture: Fixture<PanelsReduxProps> = (state = {}) => ({
  data: [panelFixture()],
  loading: true,
  ...state,
});
