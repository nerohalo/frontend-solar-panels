import { isPanelWeak } from 'helpers/panels';

describe('Test panels methods', () => {
  describe('isPanelWeak', () => {
    it('should return true if wattage is less than 200 and voltage is less than 10', () => {
      expect(isPanelWeak(199, 9)).toBeTruthy();
      expect(isPanelWeak(0, 0)).toBeTruthy();
      expect(isPanelWeak(1.99, 1.99)).toBeTruthy();
    });

    it('should return false if wattage is over 200 or voltage is over 10', () => {
      expect(isPanelWeak(200, 9)).toBeFalsy();
      expect(isPanelWeak(201, 0)).toBeFalsy();
      expect(isPanelWeak(199, 10)).toBeFalsy();
      expect(isPanelWeak(0, 11)).toBeFalsy();
    });
  });
});
