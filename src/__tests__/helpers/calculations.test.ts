import { convertWattageToKWH } from 'helpers/calculations';

describe('Test calculations methods', () => {
  describe('convertWattageToKWH', () => {
    it('should calculate kw/h correctly', () => {
      expect(convertWattageToKWH(200)).toBe(0.2);
      expect(convertWattageToKWH(1000)).toBe(1);
      expect(convertWattageToKWH(0)).toBe(0);
    });
  });
});
