import api from 'helpers/apiInstance';

import { panelsService } from 'services/panelsService';

describe('Test panelsService methods', () => {
  it('fetchPanels', async () => {
    api.get = jest.fn().mockImplementation(() => ([]));
    await panelsService.fetchPanels();
    expect(api.get).toHaveBeenCalledWith('/panels', {});
    expect(api.get).toHaveReturnedWith([]);
  });
});
