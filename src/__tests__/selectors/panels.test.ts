import dayjs from 'dayjs';

import { panelFixture, rootReducerStateFixture } from 'fixtures';
import { getOverviewData } from 'selectors/panels';

describe('getOverviewData()', () => {
  it('case 1: returns correct kw/h, strong panels, weak panels', () => {
    const state = rootReducerStateFixture({
      panels: {
        data: [
          panelFixture({ wattage: '100', voltage: '1' }),
          panelFixture({ wattage: '200', voltage: '10' }),
          panelFixture({ wattage: '200', voltage: '10' }),
        ],
        loading: false,
      },
    });

    expect(getOverviewData(state)).toEqual({
      totalKWH: '0.50',
      weakHealthPanels: 1,
      strongHealthPanels: 2,
      lastUpdated: dayjs().format('HH:mm:ss'),
    });
  });

  it('case 2: returns correct kw/h, strong panels, weak panels', () => {
    const state = rootReducerStateFixture({
      panels: {
        data: [
          panelFixture({ wattage: '100', voltage: '1' }),
          panelFixture({ wattage: '100', voltage: '10' }),
          panelFixture({ wattage: '200', voltage: '10' }),
        ],
        loading: false,
      },
    });

    expect(getOverviewData(state)).toEqual({
      totalKWH: '0.40',
      weakHealthPanels: 1,
      strongHealthPanels: 2,
      lastUpdated: dayjs().format('HH:mm:ss'),
    });
  });

  it('case 3: returns correct kw/h, strong panels, weak panels', () => {
    const state = rootReducerStateFixture({
      panels: {
        data: [
          panelFixture({ wattage: '100', voltage: '1' }),
          panelFixture({ wattage: '100', voltage: '1' }),
          panelFixture({ wattage: '200', voltage: '10' }),
        ],
        loading: false,
      },
    });

    expect(getOverviewData(state)).toEqual({
      totalKWH: '0.40',
      weakHealthPanels: 2,
      strongHealthPanels: 1,
      lastUpdated: dayjs().format('HH:mm:ss'),
    });
  });

  it('case 4: returns correct kw/h, strong panels, weak panels', () => {
    const state = rootReducerStateFixture({
      panels: {
        data: [
          panelFixture({ wattage: '200', voltage: '10' }),
          panelFixture({ wattage: '200', voltage: '10' }),
          panelFixture({ wattage: '200', voltage: '10' }),
        ],
        loading: false,
      },
    });

    expect(getOverviewData(state)).toEqual({
      totalKWH: '0.60',
      weakHealthPanels: 0,
      strongHealthPanels: 3,
      lastUpdated: dayjs().format('HH:mm:ss'),
    });
  });
});
