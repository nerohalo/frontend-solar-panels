import * as panelsActions from 'actions/panels';
import { panelFixture, rootReducerStateFixture } from 'fixtures';
import { mockPanelsService, mockServiceProvider, mockStore } from 'mocks';

describe('Test panels actions', () => {
  it('Test fetchPanels thunk', () => {
    const panel = panelFixture({ id: 1 });
    const serviceProvider = mockServiceProvider({
      panelsService: mockPanelsService({
        fetchPanels: async () => [panel],
      }),
    });

    const store = mockStore(rootReducerStateFixture({
      panels: {
        data: [],
        loading: true,
      },
    }), serviceProvider);

    store.dispatch(panelsActions.fetchPanels()).then(() => {
      const actions = store.getActions();
      expect(actions[0]).toEqual(panelsActions.panelsSet([panel]));
    });
  });
});
