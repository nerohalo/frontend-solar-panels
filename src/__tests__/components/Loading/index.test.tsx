import { render } from '@testing-library/react';
import React from 'react';

import Loading from 'components/Loading';

describe('<Loading />', () => {
  it('Renders', () => {
    const { getByTestId } = render(
      <Loading data-testid="loading" />,
    );

    expect(() => getByTestId('loading')).not.toThrow();
  });

  it('Matches snapshot', () => {
    const { baseElement } = render(
      <Loading data-testid="loading" />,
    );

    expect(baseElement).toMatchSnapshot();
  });
});
