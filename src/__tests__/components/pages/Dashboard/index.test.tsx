import { render } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';

import Dashboard from 'components/pages/Dashboard';
import { rootReducerStateFixture } from 'fixtures';
import { mockStore } from 'mocks';

jest.mock('selectors/panels', () => ({
  getOverviewData: () => ({
    totalKWH: '0.40',
    weakHealthPanels: 2,
    strongHealthPanels: 1,
    lastUpdated: '16:01:00',
  }),
}));

describe('<Dashboard />', () => {
  it('Renders', () => {
    const store = mockStore(rootReducerStateFixture());
    const { getByTestId } = render(
      <Provider store={store}>
        <Dashboard />
      </Provider>,
    );

    expect(() => getByTestId('dashboard')).not.toThrow();
  });

  it('Matches snapshot', () => {
    const store = mockStore(rootReducerStateFixture());

    const { baseElement } = render(
      <Provider store={store}>
        <Dashboard />
      </Provider>,
    );

    expect(baseElement).toMatchSnapshot();
  });
});
