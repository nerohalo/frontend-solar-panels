import { render } from '@testing-library/react';
import React from 'react';

import Cell from 'components/pages/Dashboard/Overview/Cell';

describe('<Cell />', () => {
  it('Renders all the components', () => {
    const { getByTestId, getByText } = render(
      <Cell appearance="positive" data-testid="cell">
        <Cell.Icon iconName="check_circle" />
        <Cell.Title>Healthy panels:</Cell.Title>
        <Cell.Unit>1</Cell.Unit>
      </Cell>,
    );

    expect(() => getByTestId('cell')).not.toThrow();
    expect(() => getByText('check_circle')).not.toThrow();
    expect(() => getByText('Healthy panels:')).not.toThrow();
    expect(() => getByText('1')).not.toThrow();
  });

  it('Matches snapshot', () => {
    const { baseElement } = render(
      <Cell appearance="positive" data-testid="cell">
        <Cell.Icon iconName="check_circle" />
        <Cell.Title>Healthy panels:</Cell.Title>
        <Cell.Unit>1</Cell.Unit>
      </Cell>,
    );

    expect(baseElement).toMatchSnapshot();
  });
});
