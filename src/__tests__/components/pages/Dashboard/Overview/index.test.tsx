import { render } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';

import Overview from 'components/pages/Dashboard/Overview';
import { rootReducerStateFixture } from 'fixtures';
import { mockStore } from 'mocks';

jest.mock('selectors/panels', () => ({
  getOverviewData: () => ({
    totalKWH: '0.40',
    weakHealthPanels: 2,
    strongHealthPanels: 1,
    lastUpdated: '16:01:00',
  }),
}));

describe('<Overview />', () => {
  it('Renders', () => {
    const store = mockStore(rootReducerStateFixture());
    const { getByTestId } = render(
      <Provider store={store}>
        <Overview />
      </Provider>,
    );

    expect(() => getByTestId('overview-container')).not.toThrow();
  });

  it('Renders titles', () => {
    const store = mockStore(rootReducerStateFixture());
    const { getByText } = render(
      <Provider store={store}>
        <Overview />
      </Provider>,
    );

    expect(() => getByText(/Overview/)).not.toThrow();
    expect(() => getByText(/Park Health/)).not.toThrow();
  });

  it('Renders cells', () => {
    const store = mockStore(rootReducerStateFixture());
    const { getByTestId } = render(
      <Provider store={store}>
        <Overview />
      </Provider>,
    );

    expect(() => getByTestId('overview-total-kwh-cell')).not.toThrow();
    expect(() => getByTestId('overview-updated-at-cell')).not.toThrow();
    expect(() => getByTestId('overview-healthy-panels-cell')).not.toThrow();
    expect(() => getByTestId('overview-weak-panels-cell')).not.toThrow();
  });

  it('Matches snapshot', () => {
    const store = mockStore(rootReducerStateFixture());
    const { baseElement } = render(
      <Provider store={store}>
        <Overview />
      </Provider>,
    );

    expect(baseElement).toMatchSnapshot();
  });
});
