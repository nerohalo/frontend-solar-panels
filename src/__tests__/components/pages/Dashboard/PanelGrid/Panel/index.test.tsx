import { render } from '@testing-library/react';
import React from 'react';

import Panel from 'components/pages/Dashboard/PanelGrid/Panel';
import { panelFixture } from 'fixtures';

describe('<Panel />', () => {
  it('Renders', () => {
    const { getByTestId } = render(
      <Panel panel={panelFixture({ id: 1 })} />,
    );

    expect(() => getByTestId('rendered-panel')).not.toThrow();
  });

  it('Renders', () => {
    const { getByText } = render(
      <Panel panel={panelFixture({ id: 1 })} />,
    );

    expect(() => getByText('#1')).not.toThrow();
    expect(() => getByText('250 W')).not.toThrow();
    expect(() => getByText('8033 V')).not.toThrow();
  });

  it('Matches snapshot', () => {
    const { baseElement } = render(
      <Panel panel={panelFixture({ id: 1 })} />,
    );

    expect(baseElement).toMatchSnapshot();
  });
});
