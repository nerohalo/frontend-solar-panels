import { render } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';

import PanelGrid from 'components/pages/Dashboard/PanelGrid';
import { panelFixture, rootReducerStateFixture } from 'fixtures';
import { mockStore } from 'mocks';

describe('<PanelGrid />', () => {
  it('Renders grid container', () => {
    const store = mockStore(rootReducerStateFixture({
      panels: {
        data: [],
        loading: false,
      },
    }));
    const { getByTestId } = render(
      <Provider store={store}>
        <PanelGrid />
      </Provider>,
    );

    expect(() => getByTestId('panel-grid')).not.toThrow();
  });

  it('Renders loading', () => {
    const store = mockStore(rootReducerStateFixture());
    const { getByTestId } = render(
      <Provider store={store}>
        <PanelGrid />
      </Provider>,
    );

    expect(() => getByTestId('panel-grid')).toThrow();
    expect(() => getByTestId('panel-grid-loading')).not.toThrow();
  });

  it('Renders correct amount of panels', () => {
    const store = mockStore(rootReducerStateFixture({
      panels: {
        data: [
          panelFixture({ id: 1 }),
          panelFixture({ id: 2 }),
          panelFixture({ id: 3 }),
          panelFixture({ id: 4 }),
          panelFixture({ id: 5 }),
        ],
        loading: false,
      },
    }));

    const { getAllByTestId } = render(
      <Provider store={store}>
        <PanelGrid />
      </Provider>,
    );

    const items = getAllByTestId('rendered-panel');

    expect(items).toHaveLength(5);
  });

  it('Matches snapshot', () => {
    const store = mockStore(rootReducerStateFixture({
      panels: {
        data: [
          panelFixture({ id: 1 }),
          panelFixture({ id: 2 }),
          panelFixture({ id: 3 }),
          panelFixture({ id: 4 }),
          panelFixture({ id: 5 }),
        ],
        loading: false,
      },
    }));

    const { baseElement } = render(
      <Provider store={store}>
        <PanelGrid />
      </Provider>,
    );

    expect(baseElement).toMatchSnapshot();
  });
});
