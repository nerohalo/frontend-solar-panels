import { render } from '@testing-library/react';
import React from 'react';

import NotFound from 'components/pages/NotFound';

describe('<NotFound />', () => {
  it('Renders', () => {
    const { getByText } = render(
      <NotFound />,
    );

    expect(() => getByText('Page not found!')).not.toThrow();
  });

  it('Matches snapshot', () => {
    const { baseElement } = render(
      <NotFound />,
    );

    expect(baseElement).toMatchSnapshot();
  });
});
