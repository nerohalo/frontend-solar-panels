import { ActionType } from 'typesafe-actions';

import { PanelsReduxProps } from 'types';

import c from 'helpers/constants';

import * as panelsActions from 'actions/panels';
import { panelFixture } from 'fixtures';
import reducer, { defaultState } from 'reducers/panels';

describe('Test panels reducer', () => {
  it('Should return the default state', () => {
    expect(reducer(undefined, {} as ActionType<typeof panelsActions>)).toEqual(defaultState);
  });

  it('Test PANELS_SET action', () => {
    const panels = [panelFixture({ id: 1 }), panelFixture({ id: 2 })];

    expect(
      reducer(defaultState, {
        type: c.ACTION_TYPES.PANELS_SET,
        payload: panels,
      }),
    ).toEqual({
      data: panels,
      loading: false,
    } as PanelsReduxProps);
  });
});
