import dayjs from 'dayjs';
import { createSelector } from 'reselect';

import { ApplicationState, Panel } from 'types';

import { convertWattageToKWH } from 'helpers/calculations';
import { isPanelWeak } from 'helpers/panels';

const getPanels = (state: ApplicationState): Panel[] => state.panels.data;

export const getOverviewData = createSelector(
  [getPanels],
  (panels) => {
    const totalKWHReducer = (previousValue: number, currentValue: Panel): number => previousValue + convertWattageToKWH(Number(currentValue.wattage));
    const totalKWH = panels.reduce(totalKWHReducer, 0).toFixed(2);

    const weakPanelReducer = (previousValue: number, currentValue: Panel): number => {
      if (isPanelWeak(Number(currentValue.wattage), Number(currentValue.voltage))) {
        return previousValue + 1;
      }
      return previousValue;
    };

    const weakHealthPanels = panels.reduce(weakPanelReducer, 0);
    const strongHealthPanels = panels.length - weakHealthPanels;

    const lastUpdated = dayjs().format('HH:mm:ss');

    return {
      totalKWH,
      weakHealthPanels,
      strongHealthPanels,
      lastUpdated,
    };
  },
);
